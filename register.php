<?php

require_once "config.php";

if (!empty($_POST)) {
    $error = [];
    $fileds = ["first_name", "last_name", "email", "password", "confirm_password", "country"];
    foreach ($_POST as $k => $v) {
        if (in_array($k, $fileds) && empty($v)) {
            $error[$k] = "Field $k must be filled";
        }
    }
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error['email'] = "Email is not valid";
    }
    $stmt = $dbConnect->prepare("SELECT `id` FROM `users` WHERE `email` = :email");
    $stmt->execute(["email" => $_POST['email']]);
    $isExist = $stmt->fetch();
    if (!empty($isExist)) {
        $error['email'] = "Email is already used";
    }
    if ($_POST['password'] !== $_POST['confirm_password']) {
        $error['password'] = "Password must be confirmed";
    }

    if (empty($error)) {
        //do register into db
        $stmt = $dbConnect->prepare(
            "INSERT INTO `users` 
                ( `first_name`, `last_name`, `email`, `password`, `country` )
            VALUES
                (:first_name, :last_name, :email, :password, :country)"
        );
        $stmt->execute(
            [
                "first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "email" => $_POST['email'],
                "password" => password_hash($_POST['password'], PASSWORD_DEFAULT),
                "country" => $_POST['country'],
            ]
        );
        $_SESSION['email'] = $_POST['email'];
        header("Location: /main.php");
        die();
    }
}

require_once TEMPLATE_PATH.DIRECTORY_SEPARATOR."register.php";
