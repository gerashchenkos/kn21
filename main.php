<?php

require_once "config.php";

if (empty($_SESSION['email'])) {
    if (!empty($_COOKIE['email'])) {
        $_SESSION['email'] = $_COOKIE['email'];
    } else {
        header("Location: /login.php");
        die();
    }
}

echo "Hello, " . $_SESSION['email'];
