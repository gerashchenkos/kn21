<?php

const DB_NAME = "test";
const DB_HOST = "localhost";
const DB_USER = "db_user";
const DB_USER_PASS = "1111";
define('ROOT_PATH', dirname(__FILE__));
define('TEMPLATE_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR."templates");
define('USER_EMAIL', "test@test.com");
define('USER_PASSWORD', "1111");
const COUNTRIES = [
    "ua" => "Ukraine",
    "uk" => "Great Britain",
    "pl" => "Poland",
    "de" => "Germany",
];

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dbConnect = new PDO(
    'mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_USER_PASS, [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
]
);

session_start();