<?php include_once TEMPLATE_PATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "header.php";?>
<section>
    <?php if(!empty($error)): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $error; ?>
    </div>
    <?php endif; ?>
    <form method="post" action="/login.php">
        <!-- Email input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="form2Example1">Email address</label>
            <input type="email" name="email" id="form2Example1" class="form-control" />
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="form2Example2">Password</label>
            <input type="password" name="password" id="form2Example2" class="form-control" />
        </div>

        <!-- 2 column grid layout for inline styling -->
        <div class="row mb-4">
            <div class="col d-flex justify-content-center">
                <!-- Checkbox -->
                <div class="form-check">
                    <input class="form-check-input" name="remember_me" type="checkbox" value="1" id="form2Example31" checked />
                    <label class="form-check-label" for="form2Example31"> Remember me </label>
                </div>
            </div>
        </div>

        <!-- Submit button -->
        <input type="submit" class="btn btn-primary btn-block mb-4" value="Sign in">
    </form>
</section>
<?php include_once TEMPLATE_PATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "footer.php";?>
