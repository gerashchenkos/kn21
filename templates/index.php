<?php include_once TEMPLATE_PATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "header.php";?>
<section>
    <div class="jumbotron d-flex align-items-center min-vh-100">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-4">
                    <form id="add_form" method="post" action="">
                        <div class="text-center">
                            <h2>Форма додавання товару</h2>
                        </div>
                        <div class="form_group">
                            <label for="product_id" class="form-label">Введіть номер товару</label><br/>
                            <input id="product_id" class="form-control" type="text" name="id" value="" required>
                        </div>
                        <div class="form_group">
                            <label for="name" class="form-label">Введіть назву товару</label><br/>
                            <input id="name" class="form-control" type="text" name="name" value="" required>
                        </div>
                        <div class="form_group">
                            <label for="price" class="form-label">Введіть ціну товару</label><br/>
                            <input id="price" class="form-control" type="number" name="price" min="1" max="100000"
                                   required>
                        </div>
                        <div class="form_group">
                            <label for="quantity" class="form-label">Введіть кількість товару</label><br/>
                            <input id="quantity" class="form-control" type="number" name="quantity" min="1" max="1000"
                                   required>
                        </div>
                        <div class="form_group">
                            <label for="category" class="form-label">Введіть категорію товару</label><br/>
                            <select id="category" class="form-select" name="category" required>
                                <option value="">Категорія товару</option>
                                <option value="tv">Телевізори</option>
                                <option value="phone">Телефони</option>
                                <option value="laptop">Ноутбуки</option>
                                <option value="book">Книги</option>
                            </select>
                        </div>
                        <br/>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <input class="btn btn-primary" type="submit" name="add" value="Додати">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once TEMPLATE_PATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "footer.php";?>