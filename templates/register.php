<?php

include_once TEMPLATE_PATH.DIRECTORY_SEPARATOR."partials".DIRECTORY_SEPARATOR."header.php"; ?>
<section>
    <div class="jumbotron d-flex align-items-center min-vh-100">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-4">
                    <form method="post" action="/register.php">
                        <div class="form-outline mb-4">
                            <label class="form-label" for="first_name">First Name</label>
                            <input type="text" name="first_name" id="first_name" class="form-control <?php
                            if (!empty($error['first_name'])) {
                                echo 'is-invalid';
                            } ?>"
                                   value="<?php
                                   echo($_POST['first_name'] ?? '') ?>"
                            />
                            <div class="invalid-feedback">
                                <?php
                                echo($error['first_name'] ?? ''); ?>
                            </div>
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="last_name">Last Name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control <?php
                            if (!empty($error['last_name'])) {
                                echo 'is-invalid';
                            } ?>"
                            value="<?php
                            echo ($_POST['last_name'] ?? '') ?>"/>
                            <div class="invalid-feedback">
                                <?php
                                echo($error['last_name'] ?? ''); ?>
                            </div>
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2Example1">Email address</label>
                            <input type="email" name="email" id="form2Example1" class="form-control <?php
                            if (!empty($error['email'])) {
                                echo 'is-invalid';
                            } ?>"
                                   value="<?php
                                   echo($_POST['email'] ?? '') ?>"
                            />
                            <div class="invalid-feedback">
                                <?php
                                echo($error['email'] ?? ''); ?>
                            </div>
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2Example2">Password</label>
                            <input type="password" name="password" id="form2Example2" class="form-control <?php
                            if (!empty($error['password'])) {
                                echo 'is-invalid';
                            } ?>"/>
                            <div class="invalid-feedback">
                                <?php
                                echo($error['password'] ?? ''); ?>
                            </div>
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="confirm_password">Confirm Password</label>
                            <input type="password" name="confirm_password" id="confirm_password"
                                   class="form-control <?php
                                   if (!empty($error['confirm_password'])) {
                                       echo 'is-invalid';
                                   } ?>"/>
                            <div class="invalid-feedback">
                                <?php
                                echo($error['confirm_password'] ?? ''); ?>
                            </div>
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="country">Country</label>
                            <select id="country" class="form-select <?php
                            if (!empty($error['country'])) {
                                echo 'is-invalid';
                            } ?>" name="country" required>
                                <option value="">Select Country</option>
                                <?php
                                foreach (COUNTRIES as $k => $v): ?>
                                    <option
                                            value="<?php echo $k; ?>"
                                            <?php
                                            if (!empty($_POST['country']) && $k === $_POST['country']) {
                                                echo "selected";
                                            } ?>
                                    >
                                        <?php
                                        echo $v;
                                        ?>
                                    </option>
                                <?php
                                endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?php
                                echo($error['country'] ?? ''); ?>
                            </div>
                        </div>

                        <!-- Submit button -->
                        <input type="submit" class="btn btn-primary btn-block mb-4" value="Sign in">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include_once TEMPLATE_PATH.DIRECTORY_SEPARATOR."partials".DIRECTORY_SEPARATOR."footer.php"; ?>
