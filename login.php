<?php

require_once "config.php";

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $stmt = $dbConnect->prepare("SELECT * FROM `users` WHERE `email` = :email");
    $stmt->execute(["email" => $_POST['email']]);
    $user = $stmt->fetch();
    if (!empty($user) && password_verify($_POST['password'], $user['password'])) {
        $_SESSION['email'] = $_POST['email'];
        if (!empty($_POST['remember_me'])) {
            setcookie("email", $_POST['email'], time()+3600 * 24 * 7);
        }
        header("Location: /main.php");
        die();
    } else {
        $error = "Email or password is not valid";
    }
}

require_once TEMPLATE_PATH.DIRECTORY_SEPARATOR."login.php";
